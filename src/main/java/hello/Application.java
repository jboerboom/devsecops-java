package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.apache.struts2.util.tomcat.buf.UDecoder;

@SpringBootApplication
@RestController
public class Application {

    @RequestMapping("/")
    public String home() {
        return "What an awesome DevSecOps training!" + UDecoder.URLDecode("www.google.com");
    }

    @RequestMapping("/redirect")
    public String redirect(@RequestParam("url") String url) {
        return "redirect:" + url;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
